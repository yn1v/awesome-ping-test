# AwesomePing

Pequeño script de python para hacer ping a ciertas ips. Si el ping falla envia un sms a destinario para notificar que la red no tiene acceso a internet.

## Puesta en marcha

### Requerimientos del sistema

Para que awesomePing se ejecute correctamente en necesario que se ejecute en un ambien linux y tener instalado los siguientes paquetes:

```sh
sudo dnf install python3 python3-pip gnokii uhubctl
```

### Requemientos de python

Las dependecias de esta en archivo `requirements.txt` y se pueden instalar de la siguiente manera:

```sh
pip install -r requirements.txt
```

## Configuración


En los archivos del repositorio se encuentra un archivo llamado `.config.ini_sample` debera renombrar a `.config.ini` y rellenar los datos necesarios.

### Ip
En el atributo `ip` se coloca ip a la que se desea hacer ping. Si es mas de una se separan por coma `,` sin espacio.

```ini
ip=8.8.8.8,10.10.10.10
```

### Lapso de tiempo

En el atributo `lapse` se coloca le tiempo en minutos de cada cuanto se realiza el ping. Por defecto son 5 minutos.

```ini
lapse=5
```

### Remitente

En el atributo `addressee` se colocar el numero de telefono (incluido el codigo de area) sin espacio o separadores, de a quien se notificara por mensaje de texto. Si es mas de una se separan por coma `,` sin espacio.

```ini
addressee=00100000000,00200000000
```

### Id del fabricante e Id del producto

Para las notificaciones se usa un model UBS 3G. Es necesario poder identifacar el dispocitivo y las mejor forma es con vendor id y producto id estos se peude localizar en la salida del comando `lsusb`.

```sh
lsusb

> Bus 001 Device 026: ID 19d2:0031 ZTE WCDMA Technologies MSM MF110/MF627/MF636
Bus 001 Device 003: ID 0424:ec00 Microchip Technology, Inc. (formerly SMSC) SMSC9512 9514 Fast Ethernet Adapter
Bus 001 Device 002: ID 0424:9514 Microchip Technology, Inc. (formerly SMSC) SMC9514 Hub
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
```

En nuestro caso se pueden encontrar lugo del parametro `ID` y antes del nombre del dispositivo. Estos son: **Vendor ID: 19d2** y **Product ID: 0031**.

```ini
vendor_id=19d2
product_id=_0031
```

### Puerto

Con el mismo sentido que los atributos anteriores el atributo puerto sirve para identificar el puerto USB en que esta conectato el dispositivo. El puerto se puede identificar en la salida del comando `uhubctl`.

```sh
uhubctl

Current status for hub 1-1 [0424:9514, USB 2.00, 5 ports, ppps]
  Port 1: 0503 power highspeed enable connect [0424:ec00]
  Port 2: 0100 power
  Port 3: 0100 power
  Port 4: 0100 power
  > Port 5: 0503 power highspeed enable connect [19d2:0031 ZTE,Incorporated ZTE CDMA Technologies MSM 1234567890ABCDEF]
Current status for hub 1 [1d6b:0002 Linux 6.1.0-rpi8-rpi-v6 dwc_otg_hcd DWC OTG Controller 20980000.usb, USB 2.00, 1 ports, ppps]
  Port 1: 0503 power highspeed enable connect [0424:9514, USB 2.00, 5 ports, ppps]

```

Este comando claramente expresa el puerto con la palabra **_Port_**. Idenficando el nombre del dispositivo que esta en el **Port 5:**.

```ini
device_port=5
```

> NOTA: En caso de usar un Rarpsberry Pi no es necesario llenar este atributo.

## Despliegue

AwesomePing esta pensado para funcionar como un servicio de `systemd` para aumentar la tolerancia a fallas y facilmente obter logs de la ejecucion.

### Unidad de servicio

En el repositorio se encuentra un archivo de unidad de servicio de ejemplo. Solo se necesita establecer la ruta hacia `awesomePing.py` y donde se almaceran los logs.

El atributo `ExecStart` ejecuta el comando del servicio. En nuestro caso ejecutar un script de python.

```ini
ExecStart=/usr/bin/python3 /ruta/a/tu/script/mi_script.py
```

El atributo `WorkingDirectory` locarliza el directorio de trabajo, es decir, la carpeta donde se encuentra `awesomePing.py`

```ini
WorkingDirectory=/ruta/a/tu/script
```

Los atributos `StandardOutput` y `StandardError` son las rutas donde se generar los archibos logs de la unidad de servicio. Lo mejor seria asignar un nombre diferente a cada archivo (pero tambien puede ser el mismo) y que se ubuquen en el directorio de trabajo.

```ini
StandardOutput=append:/ruta/a/tu/log/mi_script_log_d.log
StandardError=append:/ruta/a/tu/log/mi_script_log_d.log
```

Solo quedaria copiar el archivo al directorio de servicios de systemd para tu usuario en `~/.config/systemd/user/`. Recordar siempre mantener la extencion `.service`.

```sh
sudo mv mi_script.service ~/.config/systemd/user/miscriptd.service
```

> NOTA: Es posible que el directorio `~/.config/systemd/user` no exista, si este el el caso por favor creelo.

Después de crear el archivo de unidad, necesitas recargar systemd para que reconozca los cambios.

```sh
systemctl --user daemon-reload
```

Ahora podemos iniciar y habilitar el servicio.

```sh
systemctl --user start miscriptd
systemctl --user enable miscriptd
```

Cada ves que se realise un cambio en:

* El archivo de la **unidad de servicio** y se hagan efectivos los cambios se requiere:

 1) Releer los archivos de unidad.

```sh
systemctl --user daemon-reload
```

  2) Reiniciar el servicio de nuestro script.

```sh
systemctl --user restart miscriptd
```

* El archivo de la **configuracion del script** y se hagan efectivos los cambios se requiere:

  1) Reiniciar el servicio de nuestro script.

```sh
systemctl --user restart miscriptd
```

* El archivo de **codigo del script** y se hagan efectivos los cambios se requiere:

  1) Reiniciar el servicio de nuestro script.

```sh
systemctl --user restart miscriptd
```

### Logs

Aun que en el archivo de unidad de servicio ya se establecen logs AwesomePing tambien genera sus propios logs. 

En el repositorio se encuentra un archivo de configuracion de logs llamado `.logging.ini` con una conguracion por defecto lista para usarse.

Si desea puede cambiar el nombre del archivo de logs pero el directorio `.logs` no deberia cambiarse. Este es creado por defecto por `AwesomePing`.

## Asuntos

### Privilegios con PythonPing

El paquete PythonPing necesita privilegios para la lectura de puertos, para esto se asigna esa capasidad a python segun se muestra aca https://github.com/alessandromaggio/pythonping/issues/27.

```sh
$ sudo setcap cap_net_raw+ep $(readlink -f $(which python))
```

### Modem UBS 3G

El modem 3G tiene un chip de memoria que simula un cd-rom para darle los drivers a windows (seguramente obsoletos). Esto se controla via `usb-modem-switch`.

El modem 3G trata de conectarse a la red de datos de TIGO. Esto hace que no se pueda usar para enviar sms, dado que queda ocupado el recurso. Esto lo controla ModemManager y se puede parar usando `sudo systemctl stop ModemManager` o bien desabilitarlo usando `sudo systemctl disable ModemManager`.

Para PiOS se requiere la configuracion en `$HOME/.config/gnokii/` y un directorio para los logs `~/.cache/gnokii/`

### Logging

La unidad de servicio puede crear logs de tres maneras segun https://www.freedesktop.org/software/systemd/man/latest/systemd.exec.html#StandardOutput=. Eso se ve refejado de la siguiente manera.

Crear un archivo cada ves que se inicie el servicio.

```ini
StandardOutput=file:/home/user/log1.log
StandardError=file:/home/user/log2.log
```

Crear un archivo y/o a;adir informacion a un archivo existente.

```ini
StandardOutput=append:/home/user/log1.log
StandardError=append:/home/user/log2.log
```

> NOTA: Asegurarse que la ruta exista es posible que no soporte crear directorios.

### Linux USB permissions

Para el uso de uhbuctl en Linux de debe configurar una regla de udev para los permisos USB de otra manera tendras de ejecutar uhubclt como root.

Para corregir los permisos USB,  primero ejecuta `sudo uhubctl` y nota todos los `vid:pid` por hubs de los que necesites controlar. Luego, a;ade una regla udev como el archivo `/etc/udev/rules.d/52-usb.rules`. Reemplaza `2001` con el vendor id de tu hub, o remueve completamente el filtro `ATTR{idVendor}` para que cualquiera puede tener acceso a los puertos USB:

```sh
SUBSYSTEM=="usb", DRIVER=="usb", MODE="0666", ATTR{idVendor}=="2001"
# Linux 6.0 or later (its ok to have this block present for older Linux kernels):
SUBSYSTEM=="usb", DRIVER=="usb", \
  RUN="/bin/sh -c \"chmod -f 666 $sys$devpath/*-port*/disable || true\""
```

Nota que para el hub USB3, algunos hubs usan un vendor id diferente, para el USB2 vs USB3 con componentes del mismo chip, y ambos necesitan permisos para que uhubctl funcione correctamente. Por ejemplo para Raspberry Pi 4B, se necesita a;adir esta 2 lineas (o remover el filtro idVendor):

```sh
SUBSYSTEM=="usb", DRIVER=="usb", MODE="0666", ATTR{idVendor}=="2109"
SUBSYSTEM=="usb", DRIVER=="usb", MODE="0666", ATTR{idVendor}=="1d6b"
```

Si no te gusta el modo `0666`, se puede restringir el acceso por grupos:

```sh
SUBSYSTEM=="usb", DRIVER=="usb", MODE="0664", GROUP="dialout"
# Linux 6.0 or later (its ok to have this block present for older Linux kernels):
SUBSYSTEM=="usb", DRIVER=="usb", \
  RUN+="/bin/sh -c \"chown -f root:dialout $sys$devpath/*-port*/disable || true\"" \
  RUN+="/bin/sh -c \"chmod -f 660 $sys$devpath/*-port*/disable || true\""
```

Luego solo se a;aden usuario al grupo `dialout`:

```sh
sudo usermod -a -G dialout $USER
```

Para que los cambios de tu regla `udev` tomen efecto reinicia y luego corre:

```sh
sudo udevadm trigger --attr-match=subsystem=usb
```

Por aca podran encontra un archivo de regla listo para usarse https://github.com/mvp/uhubctl/blob/master/udev/rules.d/52-usb.rules.