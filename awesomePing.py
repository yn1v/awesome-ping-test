import os
import re
import sys
import usb
import time
import shelve
import logging
import argparse
import datetime
import threading
import pythonping
import subprocess
import configparser
import logging.config

CONFIG_FILE = ".config.ini"
LOGGING_FILE = ".logging.ini"
SHELVE_FILE = ".ping"
LOGS_DIR = ".logs"

parser = argparse.ArgumentParser(
    prog="AwesomePing",
    description="Realiza un ping hacia una ip y nofica si el ping no es exitoso",
)
parser.add_argument(
    "--healthy",
    action="store_true",
    help="Verifica que el modem este activo y nofica el estado del modem.",
)


class Configuration:
    config = configparser.ConfigParser()

    def __init__(self):
        self.pingConfigExist(CONFIG_FILE)
        self.loggingDirExist(LOGS_DIR)
        self.loggingConfigExist(LOGGING_FILE)

        self.config.read(CONFIG_FILE)
        logging.config.fileConfig(LOGGING_FILE)
        self.logger = logging.getLogger(self.config.get("logger", "name"))

    def pingConfigExist(self, path):
        if not os.path.exists(path) and not os.path.isfile(path):
            print(
                "El archivo de configuración no se encuentra, por favor cree el archivo de configuración y vuelva a intentarlo."
            )
            exit()

    def loggingConfigExist(self, path):
        if not os.path.exists(path) and not os.path.isfile(path):
            print(
                "El archivo de configuración para logger no se encuentra, por favor cree el archivo de configuración y vuelva a intentarlo."
            )
            exit()

    def loggingDirExist(self, path):
        if not os.path.exists(path) and not os.path.isdir(path):
            os.makedirs(path)


class Modem3GUsb:
    stopKnock = False
    modemStatus = 1

    def __init__(self, config, logger):
        self.config = config
        self.logger = logger
        self.stopKnock = False

        self.logger.info("Inicializando Modem3GUsb")
        self.getUp()

    def getUp(self):
        self.status()

        if self.modemStatus != 0:
            self.switch(False)
            self.switch(True)
            time.sleep(25)

        return True

    def status(self):
        if self.isUp():
            self.modemStatus = 0

            return self.modemStatus

        knockThread = threading.Thread(target=self.knock)
        knockThread.start()
        time.sleep(10)

        if knockThread.is_alive():
            self.stopKnock = True
            knockThread.join()

        return True

    def isUp(self):
        self.logger.debug("Verificar que el dispositivo este disponible")
        device = usb.core.find(
            idVendor=self.config.get("sms", "vendor_id"),
            idProduct=self.config.get("sms", "product_id"),
        )

        return device is not None

    def knock(self):
        while not self.stopKnock:
            self.logger.debug("Command: gnokii --identify")
            self.modemStatus = subprocess.call(
                "gnokii --identify", shell=True, executable="/bin/bash"
            )
            return self.modemStatus

        return True

    def switch(self, state):
        stateStr = {
            True: "on",
            False: "off",
        }[state]

        self.logger.debug("Command: uhubctl -l 1-1 -p 2")
        subprocess.call(
            "uhubctl -l 1-1 -p 2 -a " + stateStr,
            shell=True,
            executable="/bin/bash",
        )

        return True

    def sendMessage(self, message):
        self.getUp()

        for number in self.config.get("sms", "addressee").split(","):
            self.logger.debug("Command: gnokii --sendsms")
            commandCode = subprocess.call(
                'echo "' + message + '" | gnokii --sendsms ' + number,
                shell=True,
                executable="/bin/bash",
            )

        return commandCode

    def libertydenial(self):
        self.getUp()

        for number in self.config.get("sms", "addressee").split(","):
            self.logger.debug("Command: gnokii --sendsms")
            commandCode = subprocess.call(
                'echo "Sigo esclavisado. Atentamente Model ZTE WCDMA Technologies" | gnokii --sendsms '
                + number,
                shell=True,
                executable="/bin/bash",
            )

        if commandCode == 0:
            print("Esclavitud confirmada.")
            self.logger.info("Esclavitud confirmada.")

        exit()


class AwesomePing:
    def __init__(self, config, logger):
        self.config = config
        self.logger = logger
        self.consciente = shelve.open(SHELVE_FILE, writeback=True)

        self.logger.info("Inicializando Awesome Ping")
        self.modem = Modem3GUsb(config, logger)

    def makePing(self, ip):
        response = {
            "data": {"ip": ip},
            "at": datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"),
        }

        key = self.makeKey(ip)

        if key in self.consciente and "notified" in self.consciente[key]:
            response["notified"] = self.consciente[key]["notified"]
        else:
            response["notified"] = False

        try:
            result = pythonping.ping(ip, count=4)
        except Exception as e:
            response["message"] = (
                type(e).__name__
                + ": "
                + str(e)
                + " on line "
                + str(sys.exc_info()[-1].tb_lineno)
            )
            response["status"] = "error"
            self.logger.error(e)

            return response

        response["data"]["packets_lost"] = (
            "" + str(result.packets_lost),
        )  # The IP, Timeout Seconds

        if result.success(option=3):
            response["data"]["average"] = (
                "" + str(result.rtt_avg_ms / 10 / 100),
            )  # print average time in seconds
            response["status"] = "success"
        else:
            response["status"] = "failed"

        return response

    def pingGuard(self, ip, data):
        self.consciente[self.makeKey(ip)] = data
        self.consciente.sync()

        return True

    def makeMessage(self, data):
        message = "El ping a la ip " + data["data"]["ip"] + " ha "

        if data["status"] == "error":
            message += "producido un error con mensaje " + data["message"]
        else:
            message += "fallado con los siguientes datos: Packets lost: " + str(
                data["data"]["packets_lost"]
            )

        return message

    def isNotifiable(self, data):
        return data["status"] != "success" and not data["notified"]

    def makeKey(self, ip):
        return str(re.sub(r"\.", "", ip))

    def notify(self, ip, data):
        commandCode = self.modem.sendMessage(self.makeMessage(data))

        if commandCode == 0:
            print("-------gnokii command: Mensaje enviado exitosamente")
            self.logger.info("Notification exitosa")
            data["notified"] = True
        else:
            print("-------gnokii command: Envio del mensaje fallo.")
            self.logger.warning("Notification ha fallado")

        self.pingGuard(ip, data)

        return True

    def checkState(self, ip, data):
        key = self.makeKey(ip)

        if data["status"] == "success" and (
            key in self.consciente
            and "notified" in self.consciente[key]
            and self.consciente[key]["notified"] != "success"
        ):
            data["notified"] = False

        self.pingGuard(ip, data)

        return True


def checkIpAddress():
    lapse = int(awesomePing.config.get("ping", "lapse"))

    for item in awesomePing.config.get("ping", "ip").split(","):
        ip = item.strip()
        print("-- " + ip + " -----------------------------------------------------")

        pingData = awesomePing.makePing(ip)
        print("Ping status: " + pingData["status"])
        print("Ping notified: " + str(pingData["notified"]))

        notified = "not notified"
        if pingData["notified"]:
            notified = "Notified"

        conf.logger.info(
            "El ping a la IP: "
            + ip
            + " fue "
            + pingData["status"]
            + " con estado "
            + notified
        )

        if awesomePing.isNotifiable(pingData):
            awesomePing.notify(ip, pingData)
        else:
            awesomePing.checkState(ip, pingData)

    time.sleep(lapse * 60)
    checkIpAddress()

    return True


if __name__ == "__main__":
    conf = Configuration()
    conf.logger.info("Starting Awesome Ping")
    args = parser.parse_args()

    if args.healthy:
        conf.logger.info("Starting health check")
        modem = Modem3GUsb(conf.config, conf.logger)
        modem.libertydenial()

    awesomePing = AwesomePing(conf.config, conf.logger)

    checkIpAddress()
